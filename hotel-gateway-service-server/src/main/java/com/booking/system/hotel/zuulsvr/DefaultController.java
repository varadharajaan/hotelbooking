package com.booking.system.hotel.zuulsvr;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DefaultController {

    @GetMapping
    public String firstPage() {
        return "application successfully deployed and started...";
    }

}
