resource "google_compute_network" "hotel-network" {
  name                    = "hotel"
  auto_create_subnetworks = "false"
}

resource "google_compute_subnetwork" "hotel-subnetwork-subnet1" {
  name                     = "hotel-subnet1"
  ip_cidr_range            = "10.0.0.0/24"
  network                  = "${google_compute_network.hotel-network.self_link}"
  region                   = "us-central1"
  private_ip_google_access = true
}

resource "google_compute_subnetwork" "hotel-subnetwork-subnet2" {
  name                     = "hotel-subnet2"
  ip_cidr_range            = "10.1.0.0/24"
  network                  = "${google_compute_network.hotel-network.self_link}"
  region                   = "us-central1"
  private_ip_google_access = true
}