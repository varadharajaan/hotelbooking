provider "google" {
  credentials = "${file("../credentials/account.json")}"
  project     = "hotel-booking"
  region      = "us-central1"
}