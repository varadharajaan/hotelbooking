package com.booking.system.hotel.eurekasvr;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DefaultController {

    @GetMapping("/health")
    public String firstPage() {
        return "application successfully deployed and started...";
    }

}
