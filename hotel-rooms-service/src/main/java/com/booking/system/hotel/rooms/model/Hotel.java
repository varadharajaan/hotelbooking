package com.booking.system.hotel.rooms.model;


import lombok.*;
import lombok.experimental.SuperBuilder;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.ArrayList;
import java.util.List;

@Entity(name = "hotel")
@Getter
@Setter
@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
public class Hotel {

    @Id
    @GeneratedValue
    private String id;
    private String name;
    private String location;
    @OneToMany
    private List<Room> rooms = new ArrayList<>();
    private boolean isModified;
}
