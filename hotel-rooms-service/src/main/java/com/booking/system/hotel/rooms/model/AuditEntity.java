package com.booking.system.hotel.rooms.model;

import lombok.Getter;
import lombok.Setter;

import java.time.Instant;

@Getter
@Setter
public class AuditEntity {

    public Instant createdDate;
    public Instant modifyDate;
    public String ModifiedBy;
    public String createdBy;
}
