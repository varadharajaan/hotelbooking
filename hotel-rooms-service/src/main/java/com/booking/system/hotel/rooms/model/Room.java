package com.booking.system.hotel.rooms.model;

import lombok.*;
import lombok.experimental.SuperBuilder;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigInteger;



@Entity
@Table(name = "rooms")
@Getter
@Setter
@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
public class Room {
	
	@Id
	@Column(name = "room_id", nullable = false)
	private String roomId;

	@Column(name = "hotel_id", nullable = false)
	private String hotelId;
	
	@Column(name = "room_number", nullable = false)
	private String roomNumber;
	
	@Column(name = "rate", nullable = false)
	private BigInteger rate;
	
	@Column(name = "room_type", nullable = false)
	private String type;

	public Room withRoomId(String roomId) {
		this.setRoomId(roomId);
		return this;
	}
	public Room withHotelId(String hotelId) {
		this.setHotelId(hotelId);
		return this;
	}
	public Room withRoomNumber(String roomNumber) {
		this.setRoomNumber(roomNumber);
		return this;
	}
	public Room withRate(BigInteger rate) {
		this.setRate(rate);
		return this;
	}
	public Room withType(String type) {
		this.setType(type);
		return this;
	}
}
