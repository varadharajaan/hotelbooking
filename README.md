# HOTEL ROOM BOOKING APPLICATION 

### Technologies Used

   * Java 8   
   * Maven 3   
   * Spring Boot  
   * Spring Cloud (Config Server)  
   * Netflix (Zuul,Hystrix,Zipkin,Eureka Service Discovery) 
   * Docker and Docker Compose
   * Postgres SQL
   * ELK to monitor application (LogsPut to pull logs from docker)
 

### How to Run the Application

```bash
* git clone https://gitlab.com/varadharajaan/hotelbooking.git

* cd hotelbooking

* cd hotel-service-discovery-server && mvn spring-boot:run -Dspring-boot.run.profiles=local

* cd hotel-configuration-server && mvn spring-boot:run -Dspring-boot.run.profiles=local

* cd hotel-gateway-service-server && mvn spring-boot:run -Dspring-boot.run.profiles=local

* cd hotel-room-service-server && mvn spring-boot:run -Dspring-boot.run.profiles=local

* cd hotel-reservations-service && mvn spring-boot:run -Dspring-boot.run.profiles=local
```

```
Building the Containers
----------------------
* docker-compose up 

wait for sometime and you will see the application is UP 

```

### Endpoint for above 

```
* eureka - http://localhost:8761
* configuration server - http://localhost:8888
* gateway - http://localhost:5555
* reservation service - http://localhost:9000
* hotel service - http://localhost:8080

```

### Create Reservation

```
POST - localhost:5555/v1/{hotelId}/room/{roomId}

Request: {

"roomId" : "ROOM-001",
"hotelId : "hotel-001",
"StartDateTime" : "2020-10-10 09:00 AM IST",
"EndDateTime" : "2020-10-12 09:00 AM IST",
"guestId" : "guest-001"
}
```

### Update Reservation

```
PUT - localhost:5555/v1/{hotelId}/room/{roomId}

Request: {

"roomId" : "ROOM-001",
"hotelId : "hotel-001",
"StartDateTime" : "2020-10-10 09:00 AM IST",
"EndDateTime" : "2020-10-12 09:00 AM IST",
"guestId" : "guest-001"
}
```

## Get All Reservations for Room

```
GET - localhost:5555/v1/{hotelId}/room/{roomId}

```

## Get All Reservations for hotel

```
GET - localhost:5555/v1/{hotelId}

```

## Get Specific Reservation

```
GET - localhost:5555/v1/reservation/{reservationId}

```

## Delete Reservation

```
DELETE - localhost:5555/v1/reservation/{reservationId}

```

------------------------------------------------------------------------------------------------------------------------

Docker push to Docker Hub Container Registry
---------------------

$export PROJECT_ID="$(gcloud config get-value project -q)"

$gcloud auth configure-docker

$docker tag hotel-booking:latest "gcr.io/hotel-booking/hotel-booking:v1"

$docker push gcr.io/hotel-booking/hotel-booking:v1

------------------------------------------------------------------------------------------------------------------------
Jenkins File
--------------
```
$ Jenkins deployment which go mvn install and other stages

$ docker build and push to docker hub

$ Trigger terraform apply and provision environment and deploy the docker image in Google Kubernetes Cluster

$ Communication mail to all team members
```
------------------------------------------------------------------------------------------------------------------------
GCP Deployment using Terraform as Infrastructure as a Code
-------------------------------------

$ Step 1: terraform init
$ Step 2: terraform plan
$ Step 3: terraform apply

------------------------------------------------------------------------------------------------------------------------
    
Elastic Search-LogStash-Kibana  
--------------------------
```
$ Implemented Dockerized version of ELK to log and index all the application logs
$ To view generated logs on Kibana UI: [http://localhost:5601](http://localhost:5601)
$ Zipkin for distributed log trace
```
Custom Log Printer
--------------------------
$ Custom Log Printer which intercepts every request and response in Json Format

------------------------------------------------------------------------------------------------------------------------

Above commands gives complete details of the application

##### i. Modularise :
	For the time being I have modularise the project on package level.
	Later we can also modularise in maven modules.

------------------------------------------------------------------------------------------------------------------------
##### ii. DTO :
	It stands for Data Transfer Object.
	In this project DTO are used to send as response object from rest controller.

------------------------------------------------------------------------------------------------------------------------
##### iii. RESTful API design :
	Backend consists following restful apis i.e.

	You can find all the available api's after running the application in Swagger documentation at http://localhost:8888
------------------------------------------------------------------------------------------------------------------------
##### iv. Exception Handler via Aspect :
	Added aspect for exception handling at rest layer and send appropriate http status and error messages.

------------------------------------------------------------------------------------------------------------------------
##### v. Testing and documentation for RESTful apis:
	Integrated Swagger, for more details please visit here : http://swagger.io/
	I’ve added swagger framework for testing the apis.

------------------------------------------------------------------------------------------------------------------------
##### vi. Comments :
	Entire code styling is influenced by Clean Code principle - Robert Martin
	Which says
	'Truth can only be found in one place: the code’.
	So you may not found any comments anywhere in the project.
	Keeping in mind that git can be used to manage version of file and method name should be kept as self explanatory.

	However, if you need comments on each file. I can do that too.

------------------------------------------------------------------------------------------------------------------------
##### vii. Design principles used in Project :
	a. SOLID (single responsibility, open-closed, Liskov subsitution, interface segragation, dependency inversion) principle,
	b. Composition over inheritance,
	c. DRY(Don’t repeat yourself),
	d. Test only exceptions,
	e. KISS(Keep it simple, stupid)
	f. and some experience principle ;)


## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.
