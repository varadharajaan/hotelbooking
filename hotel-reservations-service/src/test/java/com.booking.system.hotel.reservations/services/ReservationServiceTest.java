package com.booking.system.hotel.reservations.test;

import com.booking.system.hotel.reservations.clients.RoomsFeignClient;
import com.booking.system.hotel.reservations.exception.HotelNotFoundException;
import com.booking.system.hotel.reservations.model.Hotel;
import com.booking.system.hotel.reservations.model.Reservation;
import com.booking.system.hotel.reservations.model.Room;
import com.booking.system.hotel.reservations.model.dto.ReservationRequest;
import com.booking.system.hotel.reservations.model.dto.ReservationResponse;
import com.booking.system.hotel.reservations.repository.ReservationRepository;
import com.booking.system.hotel.reservations.services.ReservationService;

import com.sun.tools.javac.util.List;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;


import static io.github.benas.randombeans.EnhancedRandomBuilder.aNewEnhancedRandom;;
import static org.mockito.ArgumentMatchers.any;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@SpringBootTest
@ActiveProfiles("local")
public class ReservationServiceTest {

    @MockBean
    private ReservationRepository reservationRepository;

    @MockBean
    private RoomsFeignClient roomsFeignClient;

    @Autowired
    private ReservationService reservationService;

    @BeforeEach
    public void init() {
        when(reservationRepository.save(any(Reservation.class))).thenReturn(aNewEnhancedRandom().nextObject(Reservation.class));
        when(roomsFeignClient.getHotel(anyString())).thenReturn(aNewEnhancedRandom().nextObject(Hotel.class));
        when(roomsFeignClient.getHotels()).thenReturn(List.of(aNewEnhancedRandom().nextObject(Hotel.class)));
        when(roomsFeignClient.getRoom(anyString(),anyString())).thenReturn(aNewEnhancedRandom().nextObject(Room.class));
        when(reservationRepository.save(any(Reservation.class))).thenReturn(aNewEnhancedRandom().nextObject(Reservation.class));

    }

    @Test
    public void  createReservation() {
        ReservationRequest reservationRequest = aNewEnhancedRandom().nextObject(ReservationRequest.class);
        final ReservationResponse reservation = reservationService.createReservation("HOTEl-001", reservationRequest);
        Assertions.assertThat(reservation.reservationId).isNotNull();
        verify(reservationRepository,times(1)).save(any(Reservation.class));
    }

    @Test
    public void createExceptionWithInvalidhotelId() {
        when(roomsFeignClient.getHotel(anyString())).thenReturn(null);
        ReservationRequest reservationRequest = aNewEnhancedRandom().nextObject(ReservationRequest.class);
        Assertions.assertThatExceptionOfType(HotelNotFoundException.class).isThrownBy(() -> reservationService.createReservation("HOTEL-001",reservationRequest));
    }

    @Test
    public void getReservationsByHotelIdAndRoomId() {
        final java.util.List<Reservation> reservationsByHotelIdAndRoomId = reservationService.getReservationsByHotelIdAndRoomId("HOTEl-001", "ROOM-001");
        Assertions.assertThat(reservationsByHotelIdAndRoomId).hasSize(1);
    }

}
