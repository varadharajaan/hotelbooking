package com.booking.system.hotel.reservations.services;

import com.booking.system.hotel.reservations.clients.RoomsFeignClient;
import com.booking.system.hotel.reservations.model.Reservation;
import com.booking.system.hotel.reservations.model.Room;
import com.booking.system.hotel.reservations.model.dto.ReservationRequest;
import com.booking.system.hotel.reservations.model.dto.ReservationResponse;
import com.booking.system.hotel.reservations.repository.ReservationRepository;
import com.booking.system.hotel.reservations.validation.ReservationVerficationHelper;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
public class ReservationServiceImpl implements ReservationService {

    private ReservationRepository reservationRepository;


    private RoomsFeignClient roomsFeignClient;

    ReservationVerficationHelper reservationVerficationHelper;

    @Autowired
    public void setReservationRepository(ReservationRepository reservationRepository) {
        this.reservationRepository = reservationRepository;
    }

    @Autowired
    public void setReservationVerficationHelper(ReservationVerficationHelper reservationVerficationHelper) {
        this.reservationVerficationHelper = reservationVerficationHelper;
    }

    @Autowired
    public void setRoomsFeignClient(RoomsFeignClient roomsFeignClient) {
        this.roomsFeignClient = roomsFeignClient;
    }


    @Override
    public ReservationResponse createReservation(final String hotelId, final ReservationRequest reservationRequest) {

        reservationVerficationHelper.verifyIfHotelvalid(hotelId);

        reservationVerficationHelper.verifyIfRoomValid(hotelId, reservationRequest);

        reservationVerficationHelper.dateIsValid(reservationRequest);

        Reservation reservation = new Reservation();

        reservation.setGuestId(reservationRequest.getGuestId());
        reservation.setEndDate(reservationRequest.getEndDate());
        reservation.setRoomId(reservationRequest.getRoomId());
        reservation.setStartDate(reservationRequest.getStartDate());

        final Reservation reservationRepo = reservationRepository.save(reservation);

        return ReservationResponse.builder()
                .guestId(reservationRepo.getGuestId())
                .reservationId(reservationRepo.getReservationId())
                .roomId(reservationRepo.getRoomId())
                .build();

    }


    @Override
    public ReservationResponse updateReservation(final String reservationId, final ReservationRequest reservationRequest) throws IllegalAccessException {

        final Reservation reservation = reservationRepository.findById(reservationId).orElseThrow(IllegalAccessException::new);

        final long seconds = Duration.between(LocalDateTime.now(), reservationRequest.getStartDate()).getSeconds();
        if (0 <= seconds) {
            throw new IllegalStateException("start date cannot be less than today's date");
        }

        reservation.setStartDate(reservationRequest.getStartDate());

        reservation.setRoomId(reservationRequest.getRoomId());

        reservation.setEndDate(reservationRequest.getEndDate());

        final Reservation reservationRepo = reservationRepository.save(reservation);

        return ReservationResponse.builder()
                .guestId(reservationRepo.getGuestId())
                .reservationId(reservationRepo.getReservationId())
                .roomId(reservationRepo.getRoomId())
                .build();

    }


    @HystrixCommand(fallbackMethod = "buildFallbackReservationList",
            // bulkhead properties
            threadPoolKey = "reservationsByHotelIdAndRoomIdThreadPool", threadPoolProperties = {
            @HystrixProperty(name = "coreSize", value = "30"),
            @HystrixProperty(name = "maxQueueSize", value = "10")})

    @Override
    public List<Reservation> getReservationsByHotelIdAndRoomId(String hotelId, String roomId) {

        Room room = roomsFeignClient.getRoom(hotelId, roomId);
        return reservationRepository.findByRoomId(roomId).stream().map(r -> r.withRoom(room))
                .collect(Collectors.toList());
    }


    private List<Reservation> buildFallbackReservationList(String hotelId, String roomId) {
        List<Reservation> fallbackList = new ArrayList<>();
        Reservation reservation = new Reservation().withReservationId("00000").withRoomId(roomId)
                .withComment("Sorry no Reservation information currently available");
        fallbackList.add(reservation);
        return fallbackList;
    }

    @Override
    public void deleteReservation(final String reservationId) {
        reservationRepository.findById(reservationId).ifPresent(it -> reservationRepository.delete(it));
    }
}