package com.booking.system.hotel.reservations.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DefaultController {

    @GetMapping
    public String firstPage() {
        return "application successfully deployed and started...";
    }

}
