package com.booking.system.hotel.reservations.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;


@ResponseStatus(HttpStatus.BAD_REQUEST)
public class RoomNotFoundException extends RuntimeException {

    /**
     *
     */
    private static final long serialVersionUID = 6601130339967844171L;

    public RoomNotFoundException() {
        super();
    }

    public RoomNotFoundException(String message) {
        super(message);
    }

}
