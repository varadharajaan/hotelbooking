package com.booking.system.hotel.reservations.validation;

import com.booking.system.hotel.reservations.clients.RoomsFeignClient;
import com.booking.system.hotel.reservations.exception.HotelNotFoundException;
import com.booking.system.hotel.reservations.exception.RoomNotFoundException;
import com.booking.system.hotel.reservations.model.Room;
import com.booking.system.hotel.reservations.model.dto.ReservationRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.Optional;

@Component
public class ReservationVerficationHelper {

    private RoomsFeignClient roomsFeignClient;

    @Autowired
    public void setRoomsFeignClient(RoomsFeignClient roomsFeignClient) {
        this.roomsFeignClient = roomsFeignClient;
    }

    public void verifyIfHotelvalid(final String hotelId) {

        Optional.ofNullable(roomsFeignClient.getHotel(hotelId))
                .orElseThrow(HotelNotFoundException::new);
    }

    public void verifyIfRoomValid(final String hotelId,final ReservationRequest reservationRequest) {
        Optional.of(reservationRequest).map(ReservationRequest::getRoomId)
                .ifPresent(it -> {
                    final Room room = roomsFeignClient.getRoom(hotelId, it);
                    if(null == room) {
                        throw new RoomNotFoundException("malformed request room not found");
                    }
                });
    }

    public void dateIsValid(final ReservationRequest reservationRequest) {
        final long seconds = Duration.between(LocalDateTime.now(), reservationRequest.getStartDate()).getSeconds();
        if(0 <= seconds) {
            throw new IllegalStateException("start date cannot be less than today's date");
        }
    }



}
