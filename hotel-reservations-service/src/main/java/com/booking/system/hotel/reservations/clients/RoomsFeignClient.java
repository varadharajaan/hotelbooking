package com.booking.system.hotel.reservations.clients;

import com.booking.system.hotel.reservations.model.Hotel;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;


import com.booking.system.hotel.reservations.model.Room;

import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;


@FeignClient("hotel-rooms-service") // client name
public interface RoomsFeignClient {

	@GetMapping(value = "v1/hotels/{hotelId}/rooms/{roomId}", consumes = APPLICATION_JSON_VALUE)
	Room getRoom(@PathVariable("hotelId") final String hotelId, @PathVariable("roomId") final String roomId);

	@GetMapping(value = "v1/hotels/{hotelId}/rooms", consumes = APPLICATION_JSON_VALUE)
	List<Room> getRooms (@PathVariable("hotelId") final String hotelId);

	@GetMapping(value = "v1/hotels/{hotelId}", consumes = APPLICATION_JSON_VALUE)
	Hotel getHotel(@PathVariable("hotelId") final String hotelId);

	@GetMapping(value = "v1/hotels", consumes = APPLICATION_JSON_VALUE)
	List<Hotel> getHotels ();
}
