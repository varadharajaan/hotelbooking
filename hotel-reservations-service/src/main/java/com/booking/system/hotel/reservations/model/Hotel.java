package com.booking.system.hotel.reservations.model;



import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@SuperBuilder
@EqualsAndHashCode
@AllArgsConstructor
public class Hotel {

    private String id;
    private String name;
    private String location;
    private List<Room> rooms = new ArrayList<>();
    private boolean isModified;
}
