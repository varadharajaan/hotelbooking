package com.booking.system.hotel.reservations.controllers;

import java.util.List;

import com.booking.system.hotel.reservations.model.dto.ReservationRequest;
import com.booking.system.hotel.reservations.model.dto.ReservationResponse;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.booking.system.hotel.reservations.model.Reservation;
import com.booking.system.hotel.reservations.services.ReservationServiceImpl;

import javax.validation.Valid;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping(value = "v1/hotels/{hotelId}/reservations")
@Slf4j
public class ReservationServiceController {

	private ReservationServiceImpl reservationService;

	@Autowired
	public void setReservationService(ReservationServiceImpl reservationService) {
		this.reservationService = reservationService;
	}

	@PostMapping
	@ApiOperation(value = "Initate Room Reservation request ", produces = APPLICATION_JSON_VALUE ,
			notes = "there is current limitation that concurrency is yet to be handled")
	public ReservationResponse createReservation(@ApiParam("hotelid room to be resevered") @PathVariable("hotelId") final String hotelId,
												 @ApiParam("request object contains reservation payload")@Valid  @RequestBody ReservationRequest reservationRequest) {

		return reservationService.createReservation(hotelId,reservationRequest);
	}

	@PutMapping(value = "/{reservationId}")
	public ReservationResponse updateReservation(@ApiParam("hotelid room to be resevered") @PathVariable("reservationId") final String reservationId,
												 @ApiParam("request object contains reservation payload for update") @Valid @RequestBody ReservationRequest reservationRequest) throws IllegalAccessException {

		return reservationService.updateReservation(reservationId,reservationRequest);
	}

	@GetMapping(value = "/{reservationId}")
	@ApiOperation(value = "get list of reservation for given room",produces = APPLICATION_JSON_VALUE)
	public List<Reservation> getReservationsForRoom(@ApiParam("hotelId ")@PathVariable("hotelId") String hotelId,
													@ApiParam("reservationId")@PathVariable("roomId") String roomId) {
		log.debug("---Entering the hotel-reservations-service ---");
		return reservationService.getReservationsByHotelIdAndRoomId(hotelId, roomId);
	}

	@GetMapping
	@ApiOperation(value = "get list of reservations for given hotel",produces = APPLICATION_JSON_VALUE)
	public List<Reservation> getAllReservationForHotel(@PathVariable("hotelId") final String hotelId) {
		log.debug("---Entering the hotel-reservations-service --- ");
		return reservationService.getReservationsByHotelIdAndRoomId(hotelId, null);
	}

	@DeleteMapping(value = "/{reservationId}")
	@ApiOperation(value = "delete reservation for given id",produces = APPLICATION_JSON_VALUE)
	public void deleteReservation(@PathVariable("reservationId") final String reservationId) {

		reservationService.deleteReservation(reservationId);

	}

}