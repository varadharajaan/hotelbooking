package com.booking.system.hotel.reservations.services;


import com.booking.system.hotel.reservations.model.Reservation;
import com.booking.system.hotel.reservations.model.dto.ReservationRequest;
import com.booking.system.hotel.reservations.model.dto.ReservationResponse;

import java.util.List;

public interface ReservationService {


    ReservationResponse createReservation(final String hotelId, final ReservationRequest reservationRequest);

    ReservationResponse updateReservation(final String reservationId, final ReservationRequest reservationRequest) throws IllegalAccessException;

    List<Reservation> getReservationsByHotelIdAndRoomId(final String hotelId, final String roomId);

    void deleteReservation(final String reservationId);
}
