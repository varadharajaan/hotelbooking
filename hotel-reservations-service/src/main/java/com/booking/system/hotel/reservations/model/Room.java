package com.booking.system.hotel.reservations.model;

import java.math.BigInteger;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder
@EqualsAndHashCode
@AllArgsConstructor
public class Room {

	private String roomId;

	private String hotelId;

	private String roomNumber;

	private BigInteger rate;

	private String type;

	@Override
	public String toString() {
		return "Room{" +
				"roomId='" + roomId + '\'' +
				", hotelId='" + hotelId + '\'' +
				", roomNumber='" + roomNumber + '\'' +
				", rate=" + rate +
				", type='" + type + '\'' +
				'}';
	}
}
