package com.booking.system.hotel.reservations.model;

import java.time.LocalDateTime;

import javax.persistence.*;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "reservations")
@Getter
@Setter
@EqualsAndHashCode
public class Reservation {

	@Id
	@GenericGenerator(name ="pass_id_gen" , strategy= "com.booking.system.hotel.reservations.model.SequenceIdGenerator")
	@Column(name = "reservation_id", nullable = false)
	private String reservationId;
	
	@Column(name = "room_id", nullable = false)
	private String roomId;
	
	@Column(name = "guest_id", nullable = false)
	private String guestId;
	
	@Column(name = "start_date", nullable = false)
	private LocalDateTime startDate;
	
	@Column(name = "end_date", nullable = false)
	private LocalDateTime endDate;
	
	@Column(name = "comment", nullable = false)
	private String comment;
	
	@Transient	
	private Room room;

	public Reservation withReservationId(String reservationId) {
		this.setReservationId(reservationId);
		return this;
	}
	public Reservation withRoomId(String roomId) {
		this.setRoomId(roomId);
		return this;
	}
	public Reservation withGuestId(String guestId) {
		this.setGuestId(guestId);
		return this;
	}

	public Reservation withComment(String comment) {
		this.setComment(comment);
		return this;
	}
	public Reservation withRoom(Room room) {
		this.setRoom(room);
		return this;
	}

}
